﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour {
    public int CountCoin;
    public GameObject PrefabCoin;
    public float AddedHeight;
    private Vector3 _size;
    private Vector3 _position;
    // Use this for initialization
    void Awake () {
         GetComponent<Terrain>();
        _size = GetComponent<Terrain>().terrainData.size;
        _position = GetComponent<Terrain>().transform.position;
	}

	void Start () {
        InstantiateCoin();
    }

    private void InstantiateCoin()
    {
        float terrainHeight = 0;
        RaycastHit hit;
        for (int i = 0; i < CountCoin; i++)
        {
            float positionX = Random.Range(_position.x, _position.x + _size.x);
            float positionZ = Random.Range(_position.z, _position.z + _size.z);

            if (Physics.Raycast(new Vector3(positionX, 9999f, positionZ),Vector3.down, out hit))
            {
                terrainHeight = hit.point.y;
            }

            float positionY = terrainHeight + AddedHeight;
            Vector3 randomPosition = new Vector3(positionX, positionY, positionZ);

            Instantiate(PrefabCoin, randomPosition, Quaternion.identity);
        }   
    }
}
