﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public static Target Instance;

    public delegate void TargetHandler();
    public static event TargetHandler TargetAchieved;

    // Use this for initialization
    void Awake () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	}

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        if (TargetAchieved == null) return;
        TargetAchieved();      
    }
}
