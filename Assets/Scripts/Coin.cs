﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public delegate void GetCoinHandler();
    public static event GetCoinHandler GetCoin;

    private AudioSource _audio;

    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;

        if (GetCoin != null)
        {
            GetCoin();
        }
        _audio.Play();
        Destroy(gameObject);
    }
}
