﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static Score Instance;

    private Text _scoreText;
    private float  _score = 0;

    public float ScoreValue { get { return _score; } }

    private void Awake()
    {
        Instance = this;
        _scoreText = GetComponent<Text>();
        Coin.GetCoin += Coin_GetCoin; ;
    }

    private void Coin_GetCoin()
    {
        _score++;
        SetScoreText();
    }

    private void Start()
    {
        _scoreText = GetComponent<Text>();
    }

    private void SetScoreText()
    {
        _scoreText.text = "COINS: " + _score.ToString();
    }


}
