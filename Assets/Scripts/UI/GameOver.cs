﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;

public class GameOver : MonoBehaviour {

    [SerializeField]
    private GameObject _joystick;

    private Text _statusGame;
    private Text _coins;

    void Start () {
        Text[] labels =  GetComponentsInChildren<Text>();
        _statusGame = labels[0];
        _coins = labels[1];

        Target.TargetAchieved += Target_TargetAchieved;
        Timer.TimeUp += Timer_TimeUp;
        gameObject.SetActive(false);
    }

    private void Timer_TimeUp()
    {
        GameOverMessage("GAME OVER", " ");
    }

    private void Target_TargetAchieved()
    {
        string coins = "COINS: " + Score.Instance.ScoreValue;
        GameOverMessage("WIN", coins);
    }

    private void GameOverMessage(string statusGame,string coins)
    {
        gameObject.SetActive(true);
        _joystick.SetActive(false);
        _statusGame.text = statusGame;
        _coins.text = coins;
    }
}
