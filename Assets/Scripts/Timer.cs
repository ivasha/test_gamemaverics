﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(UITimer))]
    public class Timer : MonoBehaviour
    {

        public GameObject alertReference;
        private UITimer _timeDisplay;
        private int _time = 60;

        public delegate void TimeUpHandler();
        public static event TimeUpHandler TimeUp;

        private void Awake()
        {
            _timeDisplay = GetComponent<UITimer>();
            Target.TargetAchieved += Target_TargetAchieved;
        }

        void Start()
        {
            InvokeRepeating("ReduceTime", 1, 1);
        }

        private void ReduceTime()
        {
            if (_time == 1)
            {
                Time.timeScale = 0;
                //  Instantiate(alertReference, new Vector3(0, 1f, -5f), transform.rotation);
                if (TimeUp != null)
                {
                    TimeUp();
                }
            }
            _time--;
            _timeDisplay.SetTextTime(_time.ToString());

        }

        private void Target_TargetAchieved()
        {
            if (_time > 0)
            {
                Time.timeScale = 0;
                CancelInvoke("ReduceTime");
            }
        }

    }
}
